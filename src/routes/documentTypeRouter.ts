import express from "express";

import { getAllDocumentTypes } from "../controllers/documentTypeController";

const router = express.Router();

/**
 * Definición de rutas de los servicios rest de tipo de documento
 */
router.get("/", getAllDocumentTypes);

export default router;