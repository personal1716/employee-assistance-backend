import express from "express";

import { addEmployee, deleteEmployee, getAllEmployee, getEmployeeById, updateEmployee } from "../controllers/employeeController";

const router = express.Router();

/**
 * Definición de rutas de los servicios rest de empleados
 */
router.get("/", getAllEmployee);
router.get("/doc", getEmployeeById);
router.post("/", addEmployee);
router.put("/", updateEmployee);
router.delete("/", deleteEmployee);

export default router;