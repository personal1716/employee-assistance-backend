import Employee from "../interfaces/Employee";
import { EmployeeService } from "../services/employeeService";
import { handleError, handleResponse } from "../util/utils";

/**
 * Agrega un empleado a la base
 * @param req Request del servicios rest
 * @param res Response del servicios rest
 */
export const addEmployee = async (req: any, res: any) => {
    try {
        let dataLogger = '';
        const document = req.body;
        const service = EmployeeService.getInstance();

        const flag = await service.getByTypeDocumentAndDocument("", document.document, document.documentType);
        if (flag) {
            dataLogger = `{ document: ${document.document}, documentType: ${document.documentType} }`;
            res.status(200).json(handleResponse('Ya existe el empleado con el documento y tipo de documento de identidad.', 100, {}, dataLogger));
            return;
        }

        const data: Employee = await service.add(document);
        if (data.id) {
            dataLogger = `{ id: ${data.id} }`;
            res.status(201).json(handleResponse('Se ha insertado el empleado exitosamente.', 201, data, dataLogger));
        } else {
            res.status(500).json(handleError('No se pudo agregar el empleado.', 500));
        }
    } catch (error) {
        res.json(handleError());
    }
}

/**
 * Obtiene todos los empleados de la base
 * @param _req Request del servicios rest
 * @param res Response del servicios rest
 */
export const getAllEmployee = async (_req: any, res: any) => {
    try {
        const service = EmployeeService.getInstance();
        const data: Employee[] = await service.getAll();
        if (data.length > 0) {
            res.status(200).json(handleResponse(`Se han encontraron ${data.length} empleados.`, 200, data));
        } else {
            res.status(200).json(handleResponse('No se encontraron empleados.', 200, []));
        }
    } catch (error) {
        res.json(handleError());
    }
}

/**
 * Obtiene el empleado buscado por id de la base
 * @param _req Request del servicios rest
 * @param res Response del servicios rest
 */
export const getEmployeeById = async (req: any, res: any) => {
    try {
        let dataLogger = '';
        const documentId = req.query.id;
        if (!documentId) {

            res.status(400).json(handleError('No se pasó el identificador por parámetro.', 400));
            return;
        }

        const service = EmployeeService.getInstance();
        const data = await service.getById(documentId);
        dataLogger = `{ id: ${documentId} }`;
        if (data) {
            res.status(200).json(handleResponse('Se ha encontrado el empleado.', 200, data, dataLogger));
        } else {
            res.status(200).json(handleResponse('No se ha encontrado el empleados.', 200, null, dataLogger));
        }
    } catch (error) {
        res.json(handleError());
    }
}

/**
 * Actualiza un empleado de la base
 * @param req Request del servicios rest
 * @param res Response del servicios rest
 */
export const updateEmployee = async (req: any, res: any) => {
    try {
        let dataLogger = '';
        const body = req.body;
        const id = body.id;

        if (!id) {
            res.status(400).json(handleError('No se pasó el identificador por parámetro.', 400));
            return;
        }
        if (!body.data) {
            res.status(400).json(handleError('No se pasó el objeto por parámetro.', 400));
            return;
        }

        const service = EmployeeService.getInstance();

        const exists = await service.getByTypeDocumentAndDocument(id, body.data.document, body.data.documentType);
        if (exists) {
            dataLogger = `{ document: ${body.data.document}, documentType: ${body.data.documentType} }`;
            res.status(200).json(handleResponse('Ya existe el empleado con el documento y tipo de documento de identidad.', 100, {}, dataLogger));
            return;
        }

        const data = await service.update(id, body.data);
        dataLogger = `{ id: ${id} }`;
        if (data) {
            res.status(200).json(handleResponse('Se ha actualizado el empleado exitosamente.', 200, data, dataLogger));
        } else {
            res.status(500).json(handleError('No se pudo actualizar el empleado.', 500));
        }
    } catch (error) {
        res.json(handleError());
    }
}

/**
 * Elimina un empleado de la base
 * @param req Request del servicios rest
 * @param res Response del servicios rest
 */
export const deleteEmployee = async (req: any, res: any) => {
    try {
        let dataLogger = '';
        const documentId = req.body.id;
        if (!documentId) res.status(400).json(handleError('No se pasó el identificador por parámetro.', 400));;
        const service = EmployeeService.getInstance();
        const data = await service.delete(documentId);
        dataLogger = `{ id: ${documentId} }`;
        if (data === 0) {
            res.status(200).json(handleError('No existe el empleado.', 200));
        } else if (data) {
            res.status(200).json(handleResponse('Se ha eliminado el empleado exitosamente.', 200, {}, dataLogger));
        } else {
            res.status(500).json(handleError('No se pudo eliminar el empleado.', 500));
        }
    } catch (error) {
        res.json(handleError());
    }
}