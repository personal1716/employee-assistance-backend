import DocumentType from "../interfaces/DocumentType";
import { DocumentTypeService } from "../services/documentTypeService";
import { handleError, handleResponse } from "../util/utils";

/**
 * Obtener todos los tipos de documentos
 * @param _req Request de servicio rest
 * @param res Response de servicio rest
 */
export const getAllDocumentTypes = async (_req: any, res: any) => {
    try {
        const service = DocumentTypeService.getInstance();
        const data: DocumentType[] = await service.getAll();
        if (data.length > 0) {
            res.status(200).json(handleResponse(`Se han encontraron ${data.length} tipos de documento.`, 200, data));
        } else {
            res.status(200).json(handleResponse('No se encontraron tipos de documento.', 200, []));
        }
    } catch (error) {
        res.json(handleError());
    }
}