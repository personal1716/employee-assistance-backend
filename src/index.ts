import cors from "cors";
import express from "express";

import config from "./config/config";
import documentTypeRouter from "./routes/documentTypeRouter";
import employeeRouter from "./routes/employeeRouter";
import logger from "./util/logger";

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/**
 * Rutas base de los servicios rest
 */
app.use("/api/employee", employeeRouter);
app.use("/api/documentType", documentTypeRouter);

const PORT = config.PORT || 3000;
app.listen(3000, () => {
    logger.info(`Servidor corriendo en el puerto ${PORT}`);
    console.log(`Server running on port ${PORT}`);
})