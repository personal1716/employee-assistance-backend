import dotenv from 'dotenv';
dotenv.config()

/**
 * Obtención de las variables de entorno.
 */
const config = {
    PORT: process.env.PORT,
    API_KEY: process.env.API_KEY,
    AUTH_DOMAIN: process.env.AUTH_DOMAIN,
    PROJECT_ID: process.env.PROJECT_ID,
    STORAGE_BUCKET: process.env.STORAGE_BUCKET,
    MESSAGIN_SENDER_ID: process.env.MESSAGIN_SENDER_ID,
    APP_ID: process.env.APP_ID,
};

export default config;