import { FirebaseApp, initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore/lite';
import config from '../config/config';

/**
 * Clase de inicialización de firebase.
 */
export default class FirebaseConfig {
    
    private static _instanceApp: FirebaseApp;

    private static configFirebase: FirebaseConfig = {
        "apiKey": config.API_KEY,
        "authDomain": config.AUTH_DOMAIN,
        "projectId": config.PROJECT_ID,
        "storageBucket": config.STORAGE_BUCKET,
        "messagingSenderId": config.MESSAGIN_SENDER_ID,
        "appId": config.APP_ID,
    };

    private constructor() { }

    /**
     * Obtiene la instancia singleton de la clase.
     * @returns instancia firebase
     */
    private static getInstance() {
        this._instanceApp = this._instanceApp ? this._instanceApp : initializeApp(this.configFirebase);
        return this._instanceApp;
    }

    /**
     * Obtiene la instancia de firestore
     * @returns firestore
     */
    static getFirestore() {
        const app = FirebaseConfig.getInstance();
        return getFirestore(app);
    }
}