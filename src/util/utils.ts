import logger from "./logger";

/**
 * Respuesta de los servicios rest
 */
interface ResponseHTTP {
    status: number;
    message: string;
    data?: any;
}

/**
 * Respuesta si salió mal la petición por parte del servidor.
 * @param message Mensaje a mostrar de respuesta del servicio rest
 * @param status Estado a mostrar de respuesta del servicio rest
 * @returns Objeto de respuesta del servicio rest
 */
export const handleError = (message: string = 'Ha ocurrido un error interno en el servidor [500].', status: number = 500) => {
    const error: ResponseHTTP = {
        status,
        message,
    };

    logger.info(`${message}`);

    return error;
}

/**
 * Respuesta si salió bien o en respusta satisfactoria a la petición por parte del servidor.
 * @param message Mensaje a mostrar de respuesta del servicio rest
 * @param status Estado a mostrar de respuesta del servicio rest
 * @param data Si retorna el servicio rest unos datos que necesitan ser mostrados en el frontend
 * @returns Objeto de respuesta del servicio rest
 */
export const handleResponse = (message: string = '', status: number = 200, data: any = null, dataLogger = '') => {
    const response: ResponseHTTP = {
        status,
        message,
        data
    };

    logger.info(`${message} (${dataLogger})`);

    return response;
}