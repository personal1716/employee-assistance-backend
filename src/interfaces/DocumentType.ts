/**
 * Interfaz para tipear los datos de tipo de documento
 */
export default interface DocumentType {
    id: string;
    name: string;
    abbreviation: string;
}