import { collection, Firestore, getDocs } from "firebase/firestore/lite";

import firebaseConfig from "../config/firebaseConfig";
import DocumentType from "../interfaces/DocumentType";

/**
 * Clase para manejar el crud con la base del tipo de documento
 */
export class DocumentTypeService {
    private nameCollection;
    private db: Firestore;
    private static _instance: DocumentTypeService;

    private constructor() {
        this.nameCollection = "documentType";
        this.db = firebaseConfig.getFirestore();
    }

    /**
     * Obtiene la instancia singleton de la clase
     * @returns Instancia de la clase
     */
    static getInstance() {
        this._instance = this._instance ? this._instance : new DocumentTypeService();
        return this._instance;
    }

    /**
     * Obtiene los datos de los tipos de documentos
     * @returns DocumentType[]
     */
    getAll = async () => {
        const collections = await getDocs(collection(this.db, this.nameCollection));
        const resp: DocumentType[] = collections.docs.map(doc => {
            return { ...doc.data() as DocumentType, id: doc.id };
        })
        return resp;
    }
}