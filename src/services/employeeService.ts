import { addDoc, collection, deleteDoc, doc, Firestore, getDoc, getDocs, query, updateDoc, where } from "firebase/firestore/lite";
import moment from "moment";

import firebaseConfig from "../config/firebaseConfig";
import Employee from "../interfaces/Employee";
import { DocumentTypeService } from "./documentTypeService";

/**
 * Clase que permite el manejo del crud con la base para los servicios de los empleados
 */
export class EmployeeService {
    private nameCollection;
    private db: Firestore;
    private static _instance: EmployeeService;

    private constructor() {
        this.nameCollection = "employee";
        this.db = firebaseConfig.getFirestore();
    }

    static getInstance() {
        this._instance = this._instance ? this._instance : new EmployeeService();
        return this._instance;
    }

    /**
     * Guarda el empleado en la base
     * @param document Objeto Empleado a guardar
     */
    add = async (document: Employee) => {
        await this.generateEmail(document);
        document.createdAt = moment().format('DD/MM/YYYY, h:mm:ss a');
        const result = await addDoc(collection(this.db, this.nameCollection), document);
        document.id = result.id;
        return document;
    }

    /**
     * Obtiene los empleados de la base
     */
    getAll = async () => {
        const collectionDocumentType = await DocumentTypeService.getInstance().getAll();
        const collections = await getDocs(collection(this.db, this.nameCollection));
        return collections.docs.map(doc => {
            const employees: Employee = doc.data() as Employee;
            const searchTypeDoc = collectionDocumentType.find(docType => docType.id === employees.documentType);
            return {
                ...employees,
                id: doc.id,
                documentType: searchTypeDoc ? searchTypeDoc.name : '',
            };
        })
    }

    /**
     * Obtiene el empleado por el parámetro buscado
     * @param id Identificador del empleado
     */
    getById = async (id: string) => {
        const docSnap = await getDoc(doc(this.db, this.nameCollection, id));
        return docSnap.exists() ? { ...docSnap.data(), id: docSnap.id } : null;
    }

    /**
     * Obtiene el empleado por los parámetros buscados
     * @param id Identificador del empleado
     * @param id Documento del empleado
     * @param id Tipo de Documento del empleado
     */
    getByTypeDocumentAndDocument = async (id: string, document: string, documentType: string) => {
        const q = query(collection(this.db, this.nameCollection), where("document", "==", document));
        const querySnapshot = await getDocs(q);
        let flag = false;
        querySnapshot.forEach((doc) => {
            const employee = doc.data() as Employee;
            if (employee.documentType === documentType && doc.id !== id) flag = true;
        });
        return flag;
    }

    /**
     * Elimina el empleado de la base
     * @param id Identificador del empleado
     */
    delete = async (id: string) => {
        const docRef = doc(this.db, this.nameCollection, id);
        let docSnap = await getDoc(docRef);
        if (!docSnap.exists()) return 0;
        await deleteDoc(docRef);
        docSnap = await getDoc(docRef);
        return docSnap.exists() ? 1 : 2;
    }

    /**
     * Actualiza el empleado de la base
     * @param id Identificador del empleado
     * @param fields Datos a actualizar
     */
    update = async (id: string, fields: any) => {
        if (fields.updateEmail) {
            await this.generateEmail(fields as Employee);
        }
        delete fields.updateEmail;
        fields.updatedAt = moment().format('DD/MM/YYYY, h:mm:ss a');
        let flag = false;
        await updateDoc(doc(this.db, this.nameCollection, id), fields).then(() => {
            flag = true;
        });
        return flag;
    }

    /**
     * Generar el correo dinámicamente
     * @param document Datos requeridos para generar el correo
     */
    async generateEmail(document: Employee) {
        const data = await this.getAll();
        const emails = data.map(item => {
            return { id: item.id, email: item.email }
        });

        const endDomain = document.countryEmployment === 'CO' ? 'co' : 'us';
        const domain = `cidenet.com.${endDomain}`;
        let currentEmail = `${document.firstName}.${document.firstLastName}@${domain}`.trim().replace(/ /g, "").toLowerCase();

        let indexUpdateEmail = 0;
        if (emails.length > 0) {
            let flag = true;
            let searchEmail;
            while (flag) {
                searchEmail = emails.find(email => email.email === currentEmail);
                if (searchEmail) {
                    indexUpdateEmail++;
                    const partEmail = currentEmail.split('@');
                    const partPoint = partEmail[0].split('.');
                    if (partPoint.length === 2) {
                        currentEmail = `${partEmail[0]}.${indexUpdateEmail}@${partEmail[1]}`;
                    } else if (partPoint.length === 3) {
                        currentEmail = `${partPoint[0]}.${partPoint[1]}.${indexUpdateEmail}@${partEmail[1]}`;
                    }
                } else {
                    flag = false;
                }
            }
        }

        document.email = currentEmail;
    }
}